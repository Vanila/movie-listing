var MovieListing = MovieListing || {};

MovieListing.Modal = function(){

  let jsonData = [];
  let nameData = {};
  let countryData = {};
  let languageData = {};
  let sortedDateData = {newYear: [], oldYear: []};
  let $main = document.getElementsByTagName('main')[0];
  var $languageHead = document.getElementsByClassName('js-language')[0];
  var $languageFoot = document.getElementsByClassName('js-language')[1]
  var $countryHead = document.getElementsByClassName('js-country')[0];
  var $countryFoot = document.getElementsByClassName('js-country')[1];
  var $yearHead = document.getElementsByClassName('js-year')[0];
  var $yearFoot = document.getElementsByClassName('js-year')[1];
  var arrayIndex = 30;

  function fetchData(){
    return new Promise ((resolve, reject) =>{
      fetch('../movie.json')
        .then((resp) =>{
          return resp.json();
        })
        .then((data) =>{
          resolve(data);
        })
        .catch((err) =>{
          reject(err);
        });
    })
  }

  function initialize(){
    fetchData()
    .then((data)=>{
      createModal(data);
    });

    window.addEventListener('scroll', _.throttle(function(event){
        triggerWhenScrolledToBottom(event);
    }, 500), false);
  }

  function triggerWhenScrolledToBottom(event){
     if ( window.innerHeight + window.scrollY + 300 > document.body.scrollHeight) {
        render(sortedDateData.newYear.slice(arrayIndex, arrayIndex+30), false);
        arrayIndex += 30;
      }
  }

  function createModal(data){
    jsonData = data;

    for(var ii = 0, n = data.length; ii < n; ii++){
      let oData = data[ii];
      let country = oData.country;
      countryData[country] = countryData[country] || [];
      countryData[country].push(oData);

      let language = oData.language;
      languageData[language] = languageData[language] || [];
      languageData[language].push(oData);

      insert(sortedDateData.oldYear, oData);
    }

    var newArr = [...sortedDateData.oldYear];
    sortedDateData.newYear = newArr.reverse();
    render(sortedDateData.newYear.slice(0, arrayIndex), true);


    populateDropdown($languageHead, languageData);
    populateDropdown($languageFoot, languageData);
    populateDropdown($countryHead, countryData);
    populateDropdown($countryFoot, countryData);
    bindEvents($languageHead, $languageFoot, languageData);
    bindEvents($languageFoot, $languageHead, languageData);
    bindEvents($countryHead, $countryFoot, countryData);
    bindEvents($countryFoot, $countryHead, countryData);
    bindEvents($yearHead, $yearFoot, sortedDateData);
    bindEvents($yearFoot, $yearHead, sortedDateData);
  }

  function render(data, shouldEmpty){
    while ($main.firstChild && shouldEmpty) {
      $main.removeChild($main.firstChild);
    }

    let temp = document.getElementsByTagName("template")[0];
    let item = temp.content.querySelector("div");
    for(var ii = 0, n = data.length; ii < n; ii++){
      let oData = data[ii];
      let div = document.importNode(item, true);

      let link = div.getElementsByTagName('a')[0];
      link.href = oData['movie_imdb_link'];
      let name = div.getElementsByClassName('name')[0];
      name.textContent += oData['movie_title'];
      let director = div.getElementsByClassName('director')[0];
      director.textContent += oData['director_name'];
      let actor = div.getElementsByClassName('actor')[0];
      actor.textContent += oData['actor_1_name'] +', '+ oData['actor_2_name'];
      let genre = div.getElementsByClassName('genre')[0];
      genre.textContent += oData['genres'].split('|').join(', ');
      let tag = div.getElementsByClassName('tag')[0];
      tag.textContent += oData['plot_keywords'].split('|').join(', ');
      let rating = div.getElementsByClassName('rating')[0];
      rating.textContent += oData['content_rating'];
      let budget = div.getElementsByClassName('budget')[0];
      budget.textContent += oData['budget'];
      let country = div.getElementsByClassName('country')[0];
      country.textContent += oData['country'];
      let language = div.getElementsByClassName('language')[0];
      language.textContent += oData['language'];
      let year = div.getElementsByClassName('year')[0];
      year.textContent += oData['title_year'];

      $main.appendChild(div);
    }
  }

  function populateDropdown($ele, data){
    for(let key in data){
      let $opt = document.createElement('option');
      $opt.value = key;
      $opt.text = key;
      $ele.appendChild($opt);
    }
  }

  function bindEvents($ele, $secondEle, data){
    $ele.addEventListener('change', (e)=>{
      let val = e.target.value;
      $secondEle.value = val;
      $secondEle.text = e.target.selectedOptions[0].text;
      let json = data[val];
      if(val === 'all'){
        json = jsonData;
      }
      render(json, true);
    });


  }

  return {
    initialize: initialize,
  };
}

var dataModal = MovieListing.Modal();

dataModal.initialize();

function insert(array, value, startVal, endVal){
  var length = array.length;
	var start = typeof(startVal) != 'undefined' ? startVal : 0;
	var end = typeof(endVal) != 'undefined' ? endVal : length - 1;
	var m = start + Math.floor((end - start)/2);

	if(length == 0){
		array.push(value);
		return;
	}

	if(value.title_year >= array[end].title_year){
		array.splice(end + 1, 0, value);
		return;
	}

	if(value.title_year <= array[start].title_year){//!!
		array.splice(start, 0, value);
		return;
	}

	if(start >= end){
		return;
	}

	if(value.title_year <= array[m].title_year){
		insert(array, value, start, m - 1);
		return;
	}

	if(value.title_year >= array[m].title_year){
		insert(array, value,  m + 1, end);
		return;
	}
}
